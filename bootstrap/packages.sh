#!/bin/sh

TASK=$1
if [ -z "$TASK" ]
then
    echo "Usage: $0 <command> [<package>]"
    echo "Commands: prepare, install"
    exit 0
fi

PACKAGE=$2

DIR="$( cd "$( dirname "$0" )" && pwd )"

if [ "$TASK" = "prepare" ]
then
	mkdir -vp "${DIR}/packages"
	if [ -z "$PACKAGE" ]
	then
		pip install --download="$DIR/packages" --requirement="$DIR/requirements.txt" --exists-action=i
	else
		pip install --download="$DIR/packages" "$PACKAGE" --exists-action=i
	fi
fi

if [ "$TASK" = "install" ]
then

	mkdir -vp "${DIR}/packages"
	export C_INCLUDE_PATH=/usr/include:/usr/local/include:/opt/local/include

	if [ -z "$PACKAGE" ]
	then
		pip install --no-index --find-links="file://$DIR/packages/" --requirement="$DIR/requirements.txt"
	else
		pip install --no-index --find-links="file://$DIR/packages/" "$PACKAGE"
	fi
fi
