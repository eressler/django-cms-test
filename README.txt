Project layout:

.
├── README.txt
├── bootstrap                 - bootstrap files and scripts
│   ├── fixtures              - directory for bootstrapping fixtures
│   ├── packages.sh           - script for handling pre-downloaded requirements
│   ├── media                 - directory for media referenced in fixtures, to be copied to MEDIA_ROOT
│   └── requirements.txt      - pip requirements file
├── docs                      - documentation directory
├── lib                       - non-project packages, not install by pip (externals, etc.)
├── manage.py                 - manage module
├── cmstest        - project root package, rename as appropriate
│   ├── __init__.py
│   ├── apps                  - package for project apps
│   │   ├── __init__.py
│   │   └── foundation        - default app for project wide models, etc.
│   │       ├── __init__.py
│   ├── conf                  - configuration package
│   │   ├── __init__.py
│   │   ├── defaults          - default settings (not meant to be changed, override instead)
│   │   │   ├── __init__.py
│   │   │   ├── settings.py
│   │   │   ├── urls.py
│   │   ├── dev               - dev server specific settings
│   │   │   ├── __init__.py
│   │   │   ├── settings.py
│   │   │   └── urls.py
│   │   ├── local             - local settings
│   │   │   ├── __init__.py
│   │   │   ├── settings.in   - version of settings file to check in, use your own settings.py file
│   │   │   └── urls.py
│   │   ├── production        - production server settings
│   │   │   ├── __init__.py
│   │   │   ├── settings.py
│   │   │   └── urls.py
│   │   ├── settings.py       - common settings
│   │   ├── urls.py           - common urls
│   ├── static                - project static files
│   │   ├── css
│   │   └── js
│   └── templates             - project templates
│       ├── includes
│       └── layouts
├── server_config             - server configs, create one per environment
│   └── dev-mod_wsgi          - sample dev settings for a mod_wsgi deployment
│   │   ├── apache.conf
│   │   ├── django.wsgi
│   │   └── setenv.sh
│   └── dev-mod_wsgi          - sample dev settings for a supervisord/gunicorn deployment
│       ├── apache.conf
│       ├── gunicorn.ini.py
│       ├── reload.sh
│       ├── setenv.sh
│       └── supervisor.ini
└── webroot                   - default static and media root, created by default settings (ignore)
    └── local.db

Usage:

django-admin.py startproject --template=/path/to/django-baseproject -e py,conf,ini,html,sh,in,wsgi testproject


