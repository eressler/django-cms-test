#!/usr/bin/env python
import os, sys

if __name__ == "__main__":
    # add lib directory to Python path
    PROJECT_DIR = os.path.realpath(os.path.dirname(__file__))
    LIBRARY_DIR = os.path.join(PROJECT_DIR, 'lib')

    sys.path.append(LIBRARY_DIR)
    sys.path.append(PROJECT_DIR)

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cmstest.conf.local.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)