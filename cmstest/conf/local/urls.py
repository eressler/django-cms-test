from django.conf.urls import patterns, include
from django.conf import settings

urlpatterns = patterns('',
   (r'', include('cmstest.conf.defaults.urls')),
   (r'', include('cmstest.conf.urls')),
)

urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)