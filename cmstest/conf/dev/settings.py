# ==================== Dev Settings ====================

# import project settings
from ..settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

# Set the allowed hosts to the host name(s) where this site will be accessible (e.g. 'dev.smashingideas.com')
ALLOWED_HOSTS = ['{{ host_name }}']

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     '',
        'USER':     '',
        'PASSWORD': '',
        'HOST':     '',
        'PORT':     '',
    }
}

ROOT_URLCONF = 'cmstest.conf.dev.urls'
