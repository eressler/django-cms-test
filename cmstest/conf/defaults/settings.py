# ==================== Default Settings ====================
"""
This file sets reasonable defaults for the main settings so that
the project and environment settings files are focused on just
what is important in that context. It should not be necessary to
edit this file on a per-project basis.

Project-wide settings go in cmstest.conf.settings
"""

import os

# Create the relevant path variables for this project:
#   MODULE_NAME = name of root project module
#   MODULE_DIR = location of the root project module
#   PROJECT_DIR = location of the project root directory
#   WEB_ROOT = root path for static and media directories
#              pull from the environment if possible, create if necessary
MODULE_NAME = 'cmstest'
MODULE_DIR  = os.path.realpath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
PROJECT_DIR = os.path.dirname(MODULE_DIR)

WEB_ROOT    = os.environ.get('DJANGO_WEB_ROOT', os.path.join(PROJECT_DIR, 'webroot'))
if not os.path.exists(WEB_ROOT):
    os.mkdir(WEB_ROOT)

MEDIA_ROOT = os.path.join(WEB_ROOT, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(WEB_ROOT, 'static')
STATIC_URL = '/static/'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

INTERNAL_IPS = (
    '127.0.0.1',
)

TIME_ZONE = 'America/Los_Angeles'

LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = False
USE_L10N = False

STATICFILES_DIRS = (
    os.path.join(MODULE_DIR, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
)

TEMPLATE_DIRS = (
    os.path.join(MODULE_DIR, 'templates'),
)

# Updated for Django 1.5
# Standard configuration used for all projects.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'smashing': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}
