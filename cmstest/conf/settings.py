# ==================== Project Settings ====================
"""
Project settings that will be used across all of the environments go here.
Any settings that should be changed from the values in
cmstest.conf.defaults.settings should be overridden here.
"""

# import default settings
from .defaults.settings import *

ADMINS = (
    ('Site Admin', 'ericr@smashingideas.com'),
)
MANAGERS = ADMINS

# generate a secret key for each site
SECRET_KEY = '=%x2wx&t%3dyzp5s1u9x3pyrqn%^%jj@3zso24(p!_1j%@iqbl'

ROOT_URLCONF = 'cmstest.conf.urls'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    'south',
    'cmstest.apps.foundation',
)