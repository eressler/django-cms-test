import multiprocessing

bind = "127.0.0.1:9090"
workers = multiprocessing.cpu_count() + 1
worker_class = "sync"
daemon = False
pidfile = '/tmp/gunicorn-cmstest-app.pid'
