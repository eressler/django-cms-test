#!/bin/bash

# Set project path for later
PROJECT_DIR=/var/www/sites/cmstest

# Activate virtual environment
source $PROJECT_DIR/env/bin/activate

# Set appropriate Django environment variables
export PYTHONPATH=$PROJECT_DIR/env/lib/python2.7/site-packages:$PROJECT_DIR/webapp/lib:$PROJECT_DIR/webapp
export DJANGO_SETTINGS_MODULE='cmstest.conf.dev.settings'
export DJANGO_WEB_ROOT=$PROJECT_DIR/webroot

# Add an alias to the project manage.py file
alias djm='python -m manage'
