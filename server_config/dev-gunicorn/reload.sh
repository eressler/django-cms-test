#!/bin/sh

# read the PID from the pidfile set in gunicorn.ini
# must be run as root or user specified in supervisord.conf
kill -HUP `cat /tmp/gunicorn-cmstest-app.pid`
