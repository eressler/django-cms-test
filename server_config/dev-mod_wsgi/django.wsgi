import os, sys
import site

site.addsitedir('/var/www/sites/cmstest/env/lib/python2.7/site-packages')
site.addsitedir('/var/www/sites/cmstest/webapp/lib')
site.addsitedir('/var/www/sites/cmstest/webapp')

sys.stdout = sys.stderr

os.environ['DJANGO_SETTINGS_MODULE'] = 'cmstest.conf.dev.settings'
os.environ['DJANGO_WEB_ROOT']        = '/var/www/sites/cmstest/webroot'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()
